package me.bramhaag.owoandroid.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import me.bramhaag.owoandroid.R;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CircularButton extends LinearLayout {

    private ImageButton button;
    private TextView text;

    private Integer buttonImage;
    private Integer buttonColor;
    private String buttonText;

    public CircularButton(Context context) {
        this(context, null, 0, 0);
    }

    public CircularButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public CircularButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CircularButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater == null) {
            throw new NullPointerException("Inflater cannot be null!");
        }

        inflater.inflate(R.layout.circular_button, this);

        processAttributes(context, attrs);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        this.button = findViewById(R.id.circular_button_button);
        this.text = findViewById(R.id.circular_button_text);

        this.button.setImageResource(this.buttonImage);
        this.button.getBackground().setTint(this.buttonColor);
        this.text.setText(this.buttonText);
    }

    @Override
    public void setOnClickListener(OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);

        //TODO java 8
        button.setOnClickListener(view -> callOnClick());
        text.setOnClickListener(view -> callOnClick());
    }

    private void processAttributes(@NotNull Context context, @NotNull AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.CircularButton);

        this.buttonImage = attributeArray.getResourceId(R.styleable.CircularButton_image, 0);
        this.buttonColor = attributeArray.getColor(R.styleable.CircularButton_backgroundColor, 0);
        this.buttonText = attributeArray.getString(R.styleable.CircularButton_text);

        attributeArray.recycle();
    }

    @NotNull
    public ImageButton getButton() {
        return button;
    }

    @NotNull
    public TextView getText() {
        return text;
    }
}
